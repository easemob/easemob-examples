package com.easemob.pubsubexamples.amqp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.impl.rabbitmq.EaseMobImpl;

public class PerfTestSender {
    public static String CLIENT_ID_PREFIX = "PERF-";
    
    public static void main(String[] args) throws Exception {
        for(int i = 0 ; i< 5 ; i++) {
            //"guest"为公用的测试用户名,密码为"guest"
            EaseMob easeMob = new EaseMobImpl("guest", "guest");
            int j = i + 20;
            String message = "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHell" + j;
            Date currentTime = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
            String currentTimeString = sdf.format(currentTime);
            
            //"testchannel"为测试用消息频道名
            easeMob.publish("testchannel1", message + "Sent On:" + currentTimeString);
            System.out.println(" [x] Sent '" + message + "'");
        
        }
    }
 }

