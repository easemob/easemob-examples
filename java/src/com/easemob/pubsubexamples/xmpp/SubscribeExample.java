package com.easemob.pubsubexamples.xmpp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.impl.xmpp.EaseMobImpl;

public class SubscribeExample {

	public static void main(String[] argv) throws Exception {
        //"guest"为测试用户名,密码为"guest"
		final EaseMob easeMob = new EaseMobImpl("jliu1", "yan7312");
		
		//客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
		//建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID，或者使用客户端APP的注册帐户名
		String clientUUID = "123456788";
		//String clientUUID = UUID.randomUUID().toString();
		
		//easeMob.subscribeToChannel("testchannel3");
		
	    //"testchannel"为测试用消息频道名
		easeMob.subscribe("testchannel3", clientUUID, new EaseMobCallBack() {
			@Override
			public void onMessage(Object message) {
				Date currentTime = new Date();
		        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
				String currentTimeString = sdf.format(currentTime);
				System.out.println(" [x] onMessage " + currentTimeString + " : " + message);
			}

            @Override
            public void onPresence(String resenceClientUUID, String presenceType) {
                System.out.println(" [x] onPresence. clientUUID: " + resenceClientUUID + ", type: " + presenceType);                
            }
            
			@Override
			public void onConnected() {
                System.out.println(" [x] onConnected");    
			}

			@Override
			public void onDisConnected() {
                System.out.println(" [x] onDisConnected");    
			}

			@Override
			public void onReConnected() {
			    System.out.println(" [x] onReConnected");				
			}

            @Override
            public void onReConnecting() {
                System.out.println(" [x] onReConnecting");      
/*                System.out.println("easeMob.isAutoReconnect():" + easeMob.isAutoReconnect());               
                easeMob.setAutoReconnect(false);
                System.out.println("easeMob.isAutoReconnect():" + easeMob.isAutoReconnect());               
                easeMob.setAutoReconnect(true);
                System.out.println("easeMob.isAutoReconnect():" + easeMob.isAutoReconnect());     */          
            }

		});
		
        //prevent exit. All smack threads are daemon threads.
		while(true) {
            Thread.currentThread().sleep(100000000);
        }
	}
}
