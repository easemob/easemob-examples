package com.easemob.pubsubexamples.xmpp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.ResourceExistsException;
import com.easemob.pubsub.impl.xmpp.EaseMobImpl;

public class PerfTest {
    public static String CLIENT_ID_PREFIX = "PERF-";
    
    public static void main(String[] args) throws Exception {
        if("-help".equalsIgnoreCase(args[0])) {
            System.out.println("Usage: java -jar easemob-java-example-0.9.jar grounp_id number_of_clients]");
            return;
        }
        String groupID = args[0]; //GroupId is a number start from 1. ie, 1,2,3 etc. Corresponding client id is PERF-1-1/PERF-1-2
        String numberOfClientString = args[1]; //GroupId is a number start from 1. ie, 1,2,3 etc. Corresponding client id is PERF-1-1/PERF-1-2
        int numberOfClient = Integer.parseInt(numberOfClientString);
        System.out.println("PerfTest Config: number_of_client [ " + numberOfClient + "], group_id [" + groupID + "]");
        PerfTest test = new PerfTest();
        test.test(groupID, numberOfClient);
    }

    void test(String groupId, int numberOfClients) throws Exception {
        Thread[] threads = new Thread[numberOfClients];
        for (int i = 0; i < threads.length; i++) {
            String id = CLIENT_ID_PREFIX + groupId + "-" + i;
            ConcurrentEaseMobClient ts = new ConcurrentEaseMobClient(id);
            Thread t = new Thread(ts);
            t.setName(id);
            t.start();
            threads[i] = t;
            
            Thread.sleep(350);
        }
        for (int i = 0; i < threads.length; i++) {
            threads[i].join();
        }
    }
    
    class ConcurrentEaseMobClient implements Runnable {
        String identity;

        ConcurrentEaseMobClient(String identity) {
            this.identity = identity;
        }

        public void run() {
          try {
            String clientUUID = identity;
            try {
                EaseMobImpl.createAccount(clientUUID, "guest");
            } catch (ResourceExistsException e) {
                //ignore
            }
              
            //测试用户名为自动产生的字符串,密码为"guest"
            EaseMob easeMob = new EaseMobImpl(clientUUID, "guest");
            
            //客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
            //建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID，或者使用客户端APP的注册帐户名
            //String clientUUID = "123456789";
            //String clientUUID = UUID.randomUUID().toString();
            
            easeMob.subscribeToChannel("testchannel3");
            
            //"testchannel"为测试用消息频道名
            easeMob.subscribe("testchannel3", clientUUID, new EaseMobCallBack() {
                @Override
                public void onMessage(Object message) {
                    Date currentTime = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
                    String currentTimeString = sdf.format(currentTime);
                    System.out.println(" [x] [" + identity + "] onMessage " + currentTimeString + " : " + message);
                    if(((String)message).contains("quit")) {
                        System.out.println(" [x] System is about to shutdown!");
                        System.exit(0);
                    }
                }

                @Override
                public void onPresence(String resenceClientUUID, String presenceType) {
                    System.out.println(" [x] [" + identity + "] onPresence. clientUUID: " + resenceClientUUID + ", type: " + presenceType);                
                }
                
                @Override
                public void onConnected() {
                    System.out.println(" [x] [" + identity + "] onConnected. ");    
                }

                @Override
                public void onDisConnected() {
                    System.out.println(" [x] [" + identity + "] onDisConnected");    
                }

                @Override
                public void onReConnected() {
                    System.out.println(" [x] [" + identity + "] onReConnected");               
                }

                @Override
                public void onReConnecting() {
                    System.out.println(" [x] [" + identity + "] onReConnecting");             
                }

            });
            
            //prevent exit. All smack threads are daemon threads.
            while(true) {
                Thread.currentThread().sleep(100000000);
            }
          } catch (Exception e) {
              e.printStackTrace();
          }
        }
     }
 }

