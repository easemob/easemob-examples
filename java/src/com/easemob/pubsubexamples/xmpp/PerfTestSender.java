package com.easemob.pubsubexamples.xmpp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.impl.xmpp.EaseMobImpl;

public class PerfTestSender {
    public static String CLIENT_ID_PREFIX = "PERF-";
    
    public static void main(String[] args) throws Exception {
        //EaseMobImpl.createAccount("guest", "guest");
        
        for(int i = 0 ; i< 1 ; i++) {
            //"guest"为公用的测试用户名,密码为"guest"
            EaseMob easeMob = new EaseMobImpl("guest", "guest");
            //easeMob.createChannel("testchannel");
            String message = "Hello";
            Date currentTime = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
            String currentTimeString = sdf.format(currentTime);
            
            //"testchannel"为测试用消息频道名
            easeMob.publish("testchannel3", message + "Sent On:" + currentTimeString);
            System.out.println(" [x] Sent '" + message + "Sent On:" + currentTimeString + "'");        
        }
    }
 }

