package com.easemob.pubsubexamples.xmpp;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.impl.xmpp.EaseMobImpl;

public class PublishExample {

	public static void main(String[] argv) throws Exception {
	    //EaseMobImpl.createAccount("uuid17", "password");

	    
        //"guest"为公用的测试用户名,密码为"guest"
		EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
		//easeMob.createChannel("testchannel3");
		String message = "Send from EaseMob. message 2. hello!";
		Date currentTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
		String currentTimeString = sdf.format(currentTime);
		
		//"testchannel"为测试用消息频道名
		easeMob.publish("testchannel3", message + "Sent On:" + currentTimeString);
		System.out.println(" [x] Sent '" + message + "Sent On:" + currentTimeString + "'");
	}

}
