EaseMob Subscribe/Publish Example
=================================

1.注册EaseMob帐户

登陆www.easemob.com，注册帐户。


2.创建应用

每个EaseMob帐户可创建一个到多个应用。


3.安全管理

3.1 为避免消息频道被滥用，消息频道的消息接收和消息发送使用2套不同的用户名/密码。在EaseMob的应用管理下可找到消息接收和消息发送对应的用户名/密码

3.2 使用SSL：TODO


4.接收消息.
EaseMob的消息广播机制可以保证每个客户端都一定可以收到推送的消息，并且同一个消息只会收到一次。如果某个客户端由于离线等原因没有收到消息，该客户端
会在与EaseMob服务器重新建立连接后收到服务器补发的漏收消息。

4.1. 创建一个EaseMob对象：

	//"guest"为公用的测试用户名,密码为"guest"
	EaseMob easeMob = new EaseMobImpl("guest", "guest");

这里你需要使用你在EaseMob帐户下创建的消息接收的用户名/密码和创建的消息频道名称。为方便测试，EaseMob缺省开通了一个公共的测试消息频道，名为"testchannel"。及测试用的公用用户"guest",密码"guest".


4.2 订阅要接收消息的消息频道，接收消息：
订阅消息频道时，每个客户端都需要提供一个唯一的客户端UUID。EaseMob需要通过该UUID来记录哪些客户端收到了推送消息，哪些客户端还没有收到推送消息。
建议使用java.util.UUID类或者通过Android DeviceID等方式来产生该UUID。

        //客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
        //建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID，或者使用客户端APP的注册帐户名
        String clientUUID = "123456789";
        //"testchannel1"为测试用消息频道名
        easeMob.subscribe("testchannel1", clientUUID, new EaseMobCallBack() {
	    @Override
	    public void onMessage(Object message) {
                System.out.println(" [x] Received '" + message + "'");
	    }

        });
		

5.广播式推送消息

5.1 创建一个EaseMob对象：

        //"guest"为公用的测试用户名,密码为"guest"
        EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
这里你需要使用你在EaseMob帐户下创建的消息发送的用户名/密码。为方便测试，EaseMob缺省开通了一个测试用的公用用户"guest",密码"guest".

5.2 向指定的消息频道推送消息：
	
        String message = "hello";
        easeMob.publish("testchannel", message);
		
为方便测试，EaseMob缺省开通了一个公共的测试消息频道，名为"testchannel"。发送的消息类型为String,也可以使用json等其他类型。


6.点对点推送消息

6.1 创建一个EaseMob对象：

        //"guest"为公用的测试用户名,密码为"guest"
        EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
这里你需要使用你在EaseMob帐户下创建的消息发送的用户名/密码。为方便测试，EaseMob缺省开通了一个测试用的公用用户"guest",密码"guest".

6.2 向指定的客户端推送消息：
	
        String message = "hello";
        easeMob.push("123456789", message);
		
"123456789"为指定的客户端的UUID。发送的消息类型为String,也可以使用json等其他类型。


7.处理网络连接错误

EaseMob针对移动设备网络连接不稳定,对流量敏感,对耗电量敏感等特性作了特定的优化,并开放了接口以便开发者做深度的定制和优化。


7.1 自动重连
你可以设置EaseMob的自动重连和自动重连延迟属性，当初始连接未能成功或成功连接后网络连接出现中断，EaseMob都会进行自动重连。

        EaseMob easeMob = new EaseMobImpl();
        easeMob.setReconnectAutomatically(true); //defaults to true
        aseMob.setReconnectDelay(30); // defaults to 5 seconds

注：自动重连缺省为打开，自动重连延时缺省为5秒钟

7.2 EaseMob与网络连接有关的回调方法：
EaseMob会在网络连接出现问题时自动重连并会调用以下方法通知客户端:

        public interface EaseMobCallBack {
    	    public void onMessage(Object message); //收到消息时会调用此方法	
            public void onConnected(); //第一次建立网络连接时会调用此方法	
            public void onDisConnected();//网络连接中断或出现异常时会调用此方法
            public void onReConnected();//网络连接恢复时会调用此方法
            public void onReConnecting();//尝试自动重连时会调用此方法
        }
	
7.3 使用Android ConnectivityManager监控网络连接状态，管理自动重连
为避免没有网络连接时盲目的自动重连，你可以使用Android ConnectivityManager监控网络连接状态，当没有网络连接时暂时关闭自动重连。

        @Override
        public void onDisConnected() {
            String cserviceName = Context.CONNECTIVITY_SERVICE;  
            ConnectivityManager cm = (ConnectivityManager) getSystemService(cserviceName);  
            //没有网络连接时不要盲目的自动重连
            if (cm.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED) { 
                easeMob.setAutoReconnect(false);
            }
        }

接收ConnectivityManager发送的消息，等待网络状态变为可连接。当然这不意味你一定可以重新和EaseMob建立连接，但至少这是一个合理的信号表明你可以重新尝试重连了。

        private BroadcastReceiver connectivityBroadcastReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
	        if(!noConnectivity) {
	            //尝试重连
	        }
	    }
        };


注:使用Android ConnectivityManager需要在AndroidManifest.xml文件中指定访问许可：

        <uses-permission android:name="android.permission.INTERNET"></uses-permission>    
        <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /> 
    
7.4 网络重连成功后重新打开自动重连

        @Override
        public void onReConnected() {
            easeMob.setAutoReconnect(true);                
        }
            
7.5 使用onReConnecting()对自动重连进行计数
你还可以用onReConnecting()对自动重连进行计数。当自动重连失败次数超过一定数量时，你可以增长自动重连的延时时间。
	
	
8.OnPresence: TODO

9.History: TODO
