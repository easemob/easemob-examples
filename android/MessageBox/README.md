EaseMob 消息盒子 Example
=================================

1.注册EaseMob帐户

登陆www.easemob.com，注册帐户。


2.创建应用

每个EaseMob帐户可创建一个到多个应用。


3.安全管理

3.1 为避免消息频道被滥用，消息频道的消息接收和消息发送使用2套不同的用户名/密码。在EaseMob的应用管理下可找到消息接收和消息发送对应的用户名/密码

3.2 使用SSL：TODO


4.导入 EaseMob SDK包到你自己的应用程序项目： 复制 amqp-client-3.0.1.jar,commons-io-1.2.jar,easemob-api-0.9.jar 到工程 libs/ 目录下


5.配置 AndroidManifest.xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.easemob.messagebox"
    android:versionCode="1"
    android:versionName="1.0" >

    <uses-sdk
        android:minSdkVersion="8"
        android:targetSdkVersion="15" />

    <application
        android:icon="@drawable/ic_launcher"
        android:name=".Gl" 
        android:label="@string/app_name"
        android:theme="@style/AppTheme" >
        <activity
            android:name="com.easemob.messagebox.MainActivity"
            android:label="@string/title_activity_main" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        
    <activity
        android:name=".settings.ManageAccountActivity"
        android:label="@string/menu_account" >
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.easemob.messagebox.MainActivity" />
    </activity>
    <activity
        android:name=".settings.ManageChannelActivity"
        android:label="@string/menu_channel" >
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.easemob.messagebox.MainActivity" />
    </activity>   
    <activity
        android:name=".settings.ViewAccountActivity"
        android:label="@string/menu_account" >
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.easemob.messagebox.MainActivity" />
    </activity>    
    <activity
        android:name=".settings.NoticeSettingActivity"
        android:label="@string/menu_settings" >
        <meta-data
            android:name="android.support.PARENT_ACTIVITY"
            android:value="com.easemob.messagebox.MainActivity" />
    </activity>                   
        <service
            android:name="com.easemob.pusheservice.EaseMobPushService">
        </service>
    </application>
    <uses-permission android:name="android.permission.INTERNET"></uses-permission>    
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.VIBRATE"/>
</manifest>


6. 加入以下代码：

6.1 在应用程序启动时调用以下代码：
    startService(new Intent(this, EaseMobPushService.class));

6.2 使用Receiver接收消息
  
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {        	
            if (EaseMobPushService.BROADCAST_ACTION.equals(intent.getAction())) {
		Bundle bundle = intent.getExtras();
	        String detailedMessage = bundle.getString(EaseMobPushService.MESSAGE);
		adapter.addEntry(detailedMessage);
	    }
        }
    };    
