package com.easemob.pusheservice;


import com.easemob.messagebox.Gl;
import com.easemob.messagebox.MainActivity;
import com.easemob.messagebox.R;
import com.easemob.pubsub.AuthenticationFailureException;
import com.easemob.pubsub.EaseMobException;
import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.impl.rabbitmq.EaseMobImpl;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class EaseMobPushService extends Service {
    private static final String TAG = EaseMobPushService.class.getSimpleName();

    
	public static final String BROADCAST_ACTION = "com.easemob.messagebox.on_receive_pushmessage";
    public static final String MESSAGE = "com.easemob.messagebox.MESSAGE";

	//private final IBinder mBinder = new EaseMobPushServiceBinder();

	Intent broadcastIntent;
	EaseMob easeMob;
	
    @Override
    public void onCreate() {
		super.onCreate();	
		broadcastIntent = new Intent(BROADCAST_ACTION);
		EaseMobSubscriberTask task = new EaseMobSubscriberTask();
	    task.execute();
	}

    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {    	
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
	    return null;
		//return mBinder;
	}

/*	public class EaseMobPushServiceBinder extends Binder {
		EaseMobPushService getService() {
			return EaseMobPushService.this;
		}
	}*/

	@Override
	public void onDestroy() {
		super.onDestroy();

		//TODO:
		//easeMob.disconnect();
	}
    
    private class EaseMobSubscriberTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... v) {            
            //configuration has not been initialized yet. Set to default values
            if(Gl.getSubscribedChannel() == null) {
                //"kfeedback"为差评修复大师公用的测试消息频道
                Gl.setSubscribedChannel("testchannel1");
            }
            if(Gl.getChannleUserName() == null) {
                //"kfeedback"为差评修复大师公用的测试用户名,密码为"kfeedback"
                Gl.setChannleUserName("guest");       
                Gl.setChannlePwd("guest");
            }
            
            
            //对于不需要对指定客户端进行单点推送的应用可以使用由系统产生的UUID，该UUID在APP第一次安装时产生 。
            //对于需要对指定客户端进行单点推送的应用可以调用EaseMob服务器提供的REST服务，传入长UUID，返回一个对应的短UUID。以方便用户手动输入该短UUID
            final String clientUUID = Gl.getDeviceUUID();
            
            final String channelName = Gl.getSubscribedChannel();
            String userName = Gl.getChannleUserName();
            String pwd = Gl.getChannlePwd();
            
            easeMob = new EaseMobImpl(userName, pwd);

            try {
                easeMob.subscribe(channelName, clientUUID, true, true, new EaseMobCallBack() {
                    @Override
                    public void onMessage(final Object message) {
                        Log.i(TAG, " [x] onMessage '" + message + "'");
                        
                        String detailedMessage = (String)message;
                        broadcastIntent.putExtra(MESSAGE, detailedMessage);
                        sendBroadcast(broadcastIntent);
                        sendNotification(detailedMessage);
                    }
                    
                    @Override
                    public void onPresence(String resenceClientUUID, String presenceType) {
                        Log.i(TAG, " [x] onPresence. clientUUID: " + resenceClientUUID + ", type: " + presenceType);

                        String detailedMessage = "";
                        if("bind".equals(presenceType)) {
                            detailedMessage = "Client " + resenceClientUUID + "上线";
                        } else if("unbind".equals(presenceType)) {
                            detailedMessage = "Client " + resenceClientUUID + "下线";
                        }

                        broadcastIntent.putExtra(MESSAGE, detailedMessage);
                        sendBroadcast(broadcastIntent);
                        sendNotification(detailedMessage);
                    }
                    
                    @Override
                    public void onConnected() {
                        Log.i(TAG, " [x] onConnected '");
                        Log.i(TAG, " [x] Subscribed to channel " + channelName + ". Client Id is " + clientUUID);
                    }

                    @Override
                    public void onDisConnected() {
                        Log.i(TAG, " [x] onDisConnected '");

                        String cserviceName = Context.CONNECTIVITY_SERVICE;  
                        ConnectivityManager cm = (ConnectivityManager) getSystemService(cserviceName);  
                        //没有网络连接时不要盲目的自动重连。停止自动重连，并监听ConnectivityManager消息，等到网络恢复
                        if (cm.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED) { 
                            easeMob.setAutoReconnect(false);
                            easeMob.closeConnection();
                            easeMob = null;
                            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);        
                            registerReceiver(connectivityBroadcastReceiver, filter);
                            Log.i(TAG, " [x] Stopped reconnecting as there is no network.  '");
                         }
                    }

                    @Override
                    public void onReConnected() {
                        Log.i(TAG, " [x] onReConnected '");
                        easeMob.setAutoReconnect(true);                
                    }

                    @Override
                    public void onReConnecting() {
                        Log.i(TAG, " [x] onReConnecting '");
                    }

                });
            } catch (AuthenticationFailureException e) {
                // TODO： 处理认证错误
                e.printStackTrace();
            } catch (EaseMobException e) {
                // TODO：
                e.printStackTrace();
            }
          
            return null;
        }
    }       
	
	public static void sendNotification(String pushMessage) {
		if (Gl.getAlertNotificationEnable()) {
			NotificationManager manager = (NotificationManager) Gl.Ct().getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = new Notification(R.drawable.ic_launcher, pushMessage, System.currentTimeMillis());

			if (Gl.getNoticedBySound()) {
				notification.defaults |= Notification.DEFAULT_SOUND;
			}
			if (Gl.getNoticedByVibrate()) {
				notification.defaults |= Notification.DEFAULT_VIBRATE;
			}
			notification.flags |= Notification.FLAG_AUTO_CANCEL;

			Intent notifyIntent = new Intent(Gl.Ct(), MainActivity.class);
			notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent contentIntent = PendingIntent.getActivity(Gl.Ct(), notification.hashCode(), notifyIntent, 0);
			notification.setLatestEventInfo(Gl.Ct(), pushMessage, pushMessage, contentIntent);
			manager.notify(notification.hashCode(), notification);
		}
	}
	
	//在没有网络连接，停止自动重连后，监听ConnectivityManager消息，如果网络恢复则重新运行EaseMobSubscriberTask
	private BroadcastReceiver connectivityBroadcastReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                return;
            }
            
	        boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
	        if(!noConnectivity) {
	            unregisterReceiver(connectivityBroadcastReceiver);
                Log.i(TAG, " [x] Re-enable auto-reconnecting as the network is restored.  '");
	            EaseMobSubscriberTask task = new EaseMobSubscriberTask();
	            task.execute();
	        }
	    }
	};
}