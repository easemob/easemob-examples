package com.easemob.messagebox.db;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;

import java.util.List;

import com.easemob.messagebox.PushMessage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteUtil {
	private DbHelper dbOpenHelper;

	public SQLiteUtil(Context context) {
		this.dbOpenHelper = new DbHelper(context);
	}

	/**
	 * 删除推送消息
	 * 
	 * @param id
	 */
	public void delPullMessageByTime(long sentTime) {
		SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
		db.execSQL("delete from " + DbHelper.TB_PUSHMESSAGE
				+ " where sentTime = ? ", new Object[] { sentTime });
		db.close();
	}

	/**
	 * 添加消息到db
	 * 
	 * @param id
	 */
	public void addPullMessageByMessageEntries(PushMessage message) {
		SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("detailedMessage", message.getMessageDetail());
		values.put("senttime", message.getSentTime().getTime());
		Long ex=db.insert(DbHelper.TB_PUSHMESSAGE, null, values);
		db.close();
	}

	/**
	 * 查询推送消息
	 * 
	 * @param id
	 */
	public ArrayList<PushMessage> findPullMessageAll() {
		ArrayList<PushMessage> pushMessageArray = new ArrayList<PushMessage>();
		SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from " + DbHelper.TB_PUSHMESSAGE+" order by senttime desc",
				null);
		while (cursor.moveToNext()) {
			PushMessage order = new PushMessage(cursor.getString(cursor
					.getColumnIndex("detailedMessage")), new Date(
					cursor.getLong(cursor.getColumnIndex("senttime"))));
			pushMessageArray.add(order);
		}
		cursor.close();
		db.close();
		return pushMessageArray;
	}
}
