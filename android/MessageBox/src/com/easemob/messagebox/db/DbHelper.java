package com.easemob.messagebox.db;

import com.easemob.messagebox.Gl;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class DbHelper extends SQLiteOpenHelper {

	public static String EASEMOBPUSHDB = "easemobpush.db";
	public static String TB_PUSHMESSAGE = "pushmessage";
	public static DbHelper dBopenHelper ;
	
	public DbHelper(Context context) {
		
		super(context, EASEMOBPUSHDB, null, 1);
		dBopenHelper=this;
	
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// db.execSQL(MyUser);
		db.execSQL(PUSH_MESSAGE); // 网络
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TB_PUSHMESSAGE);
		
		onCreate(db);
	}

	/**
	 * 
	 * 坐标记录表
	 */
	private static final String PUSH_MESSAGE = "CREATE TABLE IF NOT EXISTS "
			+ TB_PUSHMESSAGE
			+ "(detailedMessage text not null,senttime  datetime)";
	}
