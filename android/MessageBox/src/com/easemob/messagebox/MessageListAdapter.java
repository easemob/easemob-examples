package com.easemob.messagebox;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observer;
import java.util.TimeZone;

import com.easemob.messagebox.util.MessageObservable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MessageListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<PushMessage> mEntries;
	List<Boolean> mChecked;

	public MessageListAdapter(Context context, List<PushMessage> mEntries) {
		mChecked = new ArrayList<Boolean>();
		mContext = context;
		this.mEntries = mEntries;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for(PushMessage age:mEntries)
			mChecked.add(false);
	}

	@Override
	public int getCount() {
		return mEntries.size();
	}

	@Override
	public Object getItem(int position) {
		return mEntries.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null) {
			vi = mLayoutInflater.inflate(R.layout.message_list_row, null);
		}

		TextView messageDetail = (TextView) vi.findViewById(R.id.messageDetail); // message
																					// detail
		TextView sentTime = (TextView) vi.findViewById(R.id.sentTime); // time
		CheckBox selected = selected = (CheckBox) vi
				.findViewById(R.id.list_select);
		 final int index = position;
		selected.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				CheckBox cb = (CheckBox) v;
				mChecked.set(index, cb.isChecked());
				//观察者模式，通知MainActivity
				MessageObservable observable=new MessageObservable();
				observable.addObserver((Observer)mContext);
				observable.deliveryByClass(mChecked);
			}
		});
		selected.setChecked(mChecked.get(index));
		PushMessage message = mEntries.get(position);
		// Setting all values in listview
		messageDetail.setText(message.getMessageDetail());

		Date time = message.getSentTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		String currentTimeString = sdf.format(time);
		sentTime.setText(currentTimeString);
		return vi;
	}

	public void addEntry(String detailedMessage) {
		PushMessage pushMessage = new PushMessage();
		pushMessage.setMessageDetail(detailedMessage);
		pushMessage.setSentTime(new Date());
		mEntries.add(pushMessage);
		notifyDataSetChanged();
	}

}
