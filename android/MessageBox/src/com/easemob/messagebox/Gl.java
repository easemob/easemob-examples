package com.easemob.messagebox;

import com.easemob.messagebox.settings.DeviceUUIDGenerator;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Used to access global data like preference settings, permanent data and transient data  
 * It has a very short name 'Gl' abbreviated from Global.
 */
public final class Gl extends Application {
	private static final String TAG = "Gl";

	private static final String VERSION = "20004";
	private static final String PREF_FILE_NAME = "EaseMob";
	
	private static Context sContext;
	private static SharedPreferences sSharedPreferences;
	private static SharedPreferences sDefaultSharedPreferences; // settings from preference activity 

	@Override
	public void onCreate() {		
		super.onCreate();
		init(this.getApplicationContext());
	}

	public static String getVersion() {
		return VERSION;
	}
	
	/**
	 * 
	 * init
	 * @description: This method must be called in Application.onCreate() 
	 * @param context
	 */
	public static void init(Context context) {
		sContext = context;
		sDefaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // <package name>.xml
        sSharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Activity.MODE_PRIVATE); // <PREF_FILE_NAME>.xml
	}
	
	/**
	 * 
	 * Ct
	 * @description: get the application context, it can be used most where the Context is needed. 	
	 * @return the application context (comes from Application.getApplicationContext())
	 */
	public static Context Ct() {
		return sContext;
	}
	
    //for notification and alert ----------------------------------------------------------------------------------------
	//notification states
	public static final String PREF_NOTIFICATION_ENABLE = "checkbox_notification";
	private static Boolean sIsNotificationEnable = null;
	
	public static void resetNotificationEnable(){
		sIsNotificationEnable = null;
	}
	
	public static boolean getNotificationEnable(){
		if(sIsNotificationEnable == null){
			sIsNotificationEnable = sDefaultSharedPreferences.getBoolean(PREF_NOTIFICATION_ENABLE, true);
		}
		return sIsNotificationEnable;
	}
	
	//alert notification state
	public static final String PREF_ALERT_NOTIFICATION_ENABLE = "alert_checkbox_notification";
	private static Boolean sIsAlertNotificationEnable = null;
	
	public static void resetAlertNotificationEnable(){
		sIsAlertNotificationEnable = null;
	}
	
	public static boolean getAlertNotificationEnable(){
		return true;
	}
	
	//is notice by Sound
	public static final String PREF_NOTIFICATION_BY_SOUND = "checkbox_voice";
	private static Boolean sIsNoticedBySound = null;
	
	
	/**
	 * 声音提示
	 */
	public static void resetNoticedBySound(){
		sIsNoticedBySound = null;
	}
	public static boolean getNoticedBySound(){
        if(sIsNoticedBySound == null){
            sIsNoticedBySound = sDefaultSharedPreferences.getBoolean(PREF_NOTIFICATION_BY_SOUND, false);
        }
        return sIsNoticedBySound;
	}
	
	//is notice by Vibrate
	public static final String PREF_NOTIFICATION_BY_VIBRATE = "checkbox_shake";
	private static Boolean sIsNoticedByVibrate = null;
	
	/**
	 * 振动提示
	 */
	public static void resetNoticedByVibrate(){
		sIsNoticedByVibrate = null;
	}
	public static boolean getNoticedByVibrate(){
        if(sIsNoticedByVibrate == null){
            sIsNoticedByVibrate = sDefaultSharedPreferences.getBoolean(PREF_NOTIFICATION_BY_VIBRATE, false);
        }
        return sIsNoticedByVibrate;
	}
	
	
    public static final String PREF_DEVICE_UUID = "device_uuid";
    private static String deviceUUID = null;
    /**
     * 添加消息盒子号 DeviceUUID
     * @return
     */
    public static void setDeviceUUID(String deviceUUIDIn){
        //REVISIT: Can we set deviceUUID to null?
        if (deviceUUIDIn != null && !deviceUUIDIn.equals(deviceUUID)) {
            SharedPreferences.Editor editor = sDefaultSharedPreferences.edit();
            if (editor.putString(PREF_DEVICE_UUID, deviceUUIDIn).commit()) {
                deviceUUID = deviceUUIDIn;
            }
        }
    }
    
    /**
     * 获取消息盒子号 DeviceUUID
     * @return
     */
    public static String getDeviceUUID(){
        if (deviceUUID == null) {
            deviceUUID = sDefaultSharedPreferences.getString(PREF_DEVICE_UUID, null); 
            if(deviceUUID == null) {
                deviceUUID = DeviceUUIDGenerator.getDeviceUUID(sContext);
                setDeviceUUID(deviceUUID);
            } 
        }
        
        return deviceUUID;
    }
    
    
	//subscribed channel (we assume the app can only subscribe to one channel)
    public static final String PREF_SUBSCRIBED_CHANNLE = "subscribed_channel";
    private static String subscribedChannel = null;
    
    /**
     * 添加频道号
     * @param channelName
     */
    public static void setSubscribedChannel(String channelName){
        //REVISIT: Can we set channelName to null?
        if (channelName != null && !channelName.equals(subscribedChannel)) {
            SharedPreferences.Editor editor = sDefaultSharedPreferences.edit();
            if (editor.putString(PREF_SUBSCRIBED_CHANNLE, channelName).commit()) {
                subscribedChannel = channelName;
            }
        }
    }
    
    /**
     * 获取频道号
     * @return
     */
    public static String getSubscribedChannel(){
        if (subscribedChannel == null) {
            subscribedChannel = sDefaultSharedPreferences.getString(PREF_SUBSCRIBED_CHANNLE, null); 
        }
        
        return subscribedChannel;
    }
    
    //login user name
    public static final String PREF_CHANNEL_USERNAME = "channel_username";
    private static String channleUserName = null;
    
    /**
     * 添加登录名
     * @param userName
     */
    public static void setChannleUserName(String userName){
        //REVISIT: Can we set channel_username to null?
        if (userName != null && !userName.equals(channleUserName)) {
            SharedPreferences.Editor editor = sDefaultSharedPreferences.edit();
            if (editor.putString(PREF_CHANNEL_USERNAME, userName).commit()) {
                channleUserName = userName;
            }
        }
    }
    
    /**
     * 获取登录名
     * @return
     */
    public static String getChannleUserName(){
        if (channleUserName == null) {
            channleUserName = sDefaultSharedPreferences.getString(PREF_CHANNEL_USERNAME, null); 
        }
        
        return channleUserName;
    }
    
    //login pwd
    public static final String PREF_CHANNEL_PWD = "channel_pwd";
    private static String channlePwd = null;
    
    /**
     * 添加密码
     * @param pwd
     */
    public static void setChannlePwd(String pwd){
        if (pwd != null && !pwd.equals(channlePwd)) {
            SharedPreferences.Editor editor = sDefaultSharedPreferences.edit();
            if (editor.putString(PREF_CHANNEL_PWD, pwd).commit()) {
                channlePwd = pwd;
            }
        }
    }
    
    /**
     * 获取密码
     * @return
     */
    public static String getChannlePwd(){
        if (channlePwd == null) {
            channlePwd = sDefaultSharedPreferences.getString(PREF_CHANNEL_PWD, null); 
        }
        
        return channlePwd;
    }
}