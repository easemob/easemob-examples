package com.easemob.messagebox.settings;

import com.easemob.messagebox.Gl;

import com.easemob.messagebox.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ViewAccountActivity extends Activity {

    private TextView clientUUIDTextView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_account);
        
        clientUUIDTextView = (TextView)findViewById(R.id.etClientUUID);
        clientUUIDTextView.setText(Gl.getDeviceUUID());
    }
}