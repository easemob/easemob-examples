package com.easemob.messagebox.settings;

import com.easemob.messagebox.Gl;

import com.easemob.messagebox.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ManageChannelActivity extends Activity {

    private Button subscribeButton;
    private Button cancelButton;
    private EditText channelNameEditText;
    private EditText loginUserNameEditText;
    private EditText pwdEditText;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_channel);
        
        subscribeButton = (Button) findViewById(R.id.bnSubscribe);
        cancelButton = (Button) findViewById(R.id.bnCancel);
        
        channelNameEditText = (EditText)findViewById(R.id.etChannelName);
        String savedChannelName = Gl.getSubscribedChannel();
        if(savedChannelName != null) {
            channelNameEditText.setText(savedChannelName);
        }
        
        loginUserNameEditText = (EditText)findViewById(R.id.etLoginUsername);
        String savedLoginUserName = Gl.getChannleUserName();
        if(savedLoginUserName != null) {
            loginUserNameEditText.setText(savedLoginUserName);
        }  
        
        pwdEditText = (EditText)findViewById(R.id.etPwd);
        String savedPwd = Gl.getChannleUserName();
        if(savedPwd != null) {
            pwdEditText.setText(savedPwd);
        }   
                
        subscribeButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gl.setSubscribedChannel(channelNameEditText.getText().toString());      
                Gl.setChannleUserName(loginUserNameEditText.getText().toString());
                Gl.setChannlePwd(pwdEditText.getText().toString());
                
                //TODO: use the channel name to subscribe. How to subscribe multiple channels?
                Toast.makeText(Gl.Ct(), R.string.channel_subscribed_successfully, Toast.LENGTH_SHORT).show();
                finish();
            }            
        });

        cancelButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();                
            }            
        });
    }
}