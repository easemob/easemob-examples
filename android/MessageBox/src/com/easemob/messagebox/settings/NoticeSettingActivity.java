package com.easemob.messagebox.settings;


import com.easemob.messagebox.Gl;

import com.easemob.messagebox.R;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class NoticeSettingActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	private static final String TAG = "NoticeSettingActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.notice_setting);
	}

	@Override
	protected void onResume() {
	    super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
	    super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(Gl.PREF_NOTIFICATION_ENABLE)) {
			Gl.resetNotificationEnable();
		} else if (key.equals(Gl.PREF_ALERT_NOTIFICATION_ENABLE)) {
			Gl.resetAlertNotificationEnable();
		} else if (key.equals(Gl.PREF_NOTIFICATION_BY_SOUND)) {
			Gl.resetNoticedBySound();
		} else if (key.equals(Gl.PREF_NOTIFICATION_BY_VIBRATE)) {
			Gl.resetNoticedByVibrate();
		}
	}
}
