package com.easemob.messagebox;

import java.util.Date;

public class PushMessage {
	private String messageDetail;
	private Date sentTime;
	private String sender;
	private String type;
	public PushMessage(){}
	public PushMessage(String messageDetail,Date sentTime){
		this.messageDetail=messageDetail;
		this.sentTime=sentTime;
	}
	public String getMessageDetail() {
		return messageDetail;
	}
	public void setMessageDetail(String messageDetail) {
		this.messageDetail = messageDetail;
	}
	public Date getSentTime() {
		return sentTime;
	}
	public void setSentTime(Date sentTime) {
		this.sentTime = sentTime;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }		
}
