package com.easemob.messagebox;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import com.easemob.messagebox.db.SQLiteUtil;
import com.easemob.messagebox.settings.ManageChannelActivity;
import com.easemob.messagebox.settings.NoticeSettingActivity;
import com.easemob.messagebox.settings.ViewAccountActivity;
import com.easemob.pusheservice.EaseMobPushService;
import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener,
		OnItemLongClickListener, Observer {
	MessageListAdapter adapter = null;
	Button testMessageBut;
	private ListView messageLv;
	private SQLiteUtil sqlUtil;
	private List<PushMessage> mEntries;
	private LinearLayout delMessageLayout;
	ArrayList<Boolean> mChecked;// 批量删除时的数据id统计

	/**
	 * 删除消息
	 */
	private View delDialogView;
	private Dialog delDialog;
	private ImageView okDelIv, canDelIv;
	private int arg2 = 0;// 单条删除id

	private static int DELSTATE = 0;// 0单条、1多条

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_main);
		initUI();
		initLinstener();
		initData();

		doStartService();
		registerReceiver(broadcastReceiver, new IntentFilter(
				EaseMobPushService.BROADCAST_ACTION));

	}

	void initUI() {
		delDialogView = LayoutInflater.from(this).inflate(
				R.layout.view_del_dialog, null);
		okDelIv = (ImageView) delDialogView.findViewById(R.id.alert_ok);
		canDelIv = (ImageView) delDialogView.findViewById(R.id.alert_canl);
		delDialog = new Dialog(this, R.style.progress_dialog);
		delDialog.setContentView(delDialogView);
		LayoutParams params = delDialog.getWindow().getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		delDialog.getWindow().setAttributes(params);
		delMessageLayout = (LinearLayout) findViewById(R.id.main_del_message_ly);
		messageLv = (ListView) findViewById(R.id.message_lv);
		//testMessageBut = (Button) findViewById(R.id.test_message);
	}

	void initLinstener() {
		okDelIv.setOnClickListener(this);
		canDelIv.setOnClickListener(this);
		//testMessageBut.setOnClickListener(this);
		messageLv.setOnItemLongClickListener(this);
		delMessageLayout.setOnClickListener(this);
	}

	void initData() {
		mEntries = new ArrayList<PushMessage>();
		sqlUtil = new SQLiteUtil(this);
		updateAdapter();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (broadcastReceiver != null) {
			unregisterReceiver(broadcastReceiver);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent noticeSettingIntent = new Intent(this,
					NoticeSettingActivity.class);
			startActivity(noticeSettingIntent);
			break;
		case R.id.menu_account:
			Intent accountIntent = new Intent(this, ViewAccountActivity.class);
			startActivity(accountIntent);
			break;
		case R.id.menu_channel:
			Intent channelIntent = new Intent(this, ManageChannelActivity.class);
			startActivity(channelIntent);
			break;
		}
		return true;
	}

	void doStartService() {
		startService(new Intent(this, EaseMobPushService.class));
	}

	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (EaseMobPushService.BROADCAST_ACTION.equals(intent.getAction())) {
				Bundle bundle = intent.getExtras();
				String detailedMessage = bundle.getString(EaseMobPushService.MESSAGE);
				PushMessage pushMessage = new PushMessage(detailedMessage, new Date());
	            sqlUtil.addPullMessageByMessageEntries(pushMessage);
	            delMessageLayout.setVisibility(View.GONE);
	            updateAdapter();
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
/*		case R.id.test_message:
			PushMessage pushMessage = new PushMessage(
					"前在路口把车速降至极低，但这给本身拥堵不堪的交通继续添堵；要么随时准备灯前急刹，但这已经造成更多追尾事故，违背牛顿第一定律，而制定的这条规定实在不", new Date());
			sqlUtil.addPullMessageByMessageEntries(pushMessage);
			updateAdapter();
			delMessageLayout.setVisibility(View.GONE);
			break;*/
		case R.id.main_del_message_ly:
			DELSTATE=1;
			delDialog.show();
			break;
		case R.id.alert_ok:
			if (DELSTATE == 0) {//删除单条
				sqlUtil.delPullMessageByTime(mEntries.get(arg2).getSentTime()
						.getTime());
				updateAdapter();
			} else {//删除多条
				if (mChecked == null)
					return;
				for (int i = 0; i < mChecked.size(); i++) {
					if (mChecked.get(i)) {
						sqlUtil.delPullMessageByTime(mEntries.get(i)
								.getSentTime().getTime());
					}

				}
				updateAdapter();
			}
			delMessageLayout.setVisibility(View.GONE);
			delDialog.hide();
			break;
		case R.id.alert_canl:
			delDialog.hide();
		default:
			break;
		}

	}

	/**
	 * 更新数据
	 */
	void updateAdapter() {
		mEntries = sqlUtil.findPullMessageAll();
		adapter = new MessageListAdapter(MainActivity.this, mEntries);
		if (adapter != null)
			messageLv.setAdapter(adapter);
		else
			messageLv.setAdapter(adapter);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
			final int arg2, long arg3) {
		delDialog.show();
		this.arg2 = arg2;
		this.DELSTATE=0;
		return false;
	}

	@Override
	public void update(Observable observable, Object data) {
		/**
		 * 如果有选择，那么显示删除menu
		 */
		mChecked = (ArrayList<Boolean>) (((Object[]) data)[0]);
		if (mChecked != null) {
			for (int i = 0; i < mChecked.size(); i++) {
				if (mChecked.get(i)) {
					delMessageLayout.setVisibility(View.VISIBLE);
					return;
				}
			}
		}

		/**
		 * 选择了取消，并且没有一个选中那么将齐隐藏掉
		 */
		delMessageLayout.setVisibility(View.GONE);

	}
}
