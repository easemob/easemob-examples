package com.easemob.messagebox.util;

import java.util.ArrayList;
import java.util.Observable;

import android.R.integer;
import android.annotation.SuppressLint;

@SuppressLint("ParserError")
public class MessageObservable extends Observable {
	public void deliveryByClass(Object... o) {
		this.setChanged();
		this.notifyObservers(o);
	}


}
