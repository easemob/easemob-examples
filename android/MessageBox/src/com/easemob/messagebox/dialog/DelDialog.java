package com.easemob.messagebox.dialog;



import com.easemob.messagebox.R;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;

@SuppressLint({ "ParserError", "ParserError" })
public class DelDialog {
	Dialog dialog;
	Context context;

	public DelDialog(Context c) {
		this.context = c;
		dialog = new Dialog(context, R.style.progress_dialog);
		View view = LayoutInflater.from(context).inflate(
				R.layout.view_del_dialog, null);
		dialog.setContentView(view);
		LayoutParams params = dialog.getWindow().getAttributes();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		dialog.getWindow().setAttributes(params);
	}

	public void show() {
		if (dialog != null && dialog.isShowing()) {
			dialog.show();
		} else {
			dialog.hide();
			dialog.show();
		}
	}

	public void hide() {
		if (dialog != null) {
			dialog.hide();
		}
	}
	public void canl(){
		if (dialog != null) {
			dialog.cancel();
		}
	}
}
